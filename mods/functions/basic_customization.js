"use strict";
exports.__esModule = true;
var functions_1 = require("./functions");
var sleep_1 = require("./sleep");
var readline = require('readline-sync');
function startScreen() {
    console.log('############### THE LEGEND OF ZELDA ###############');
    console.log('                   HYRULE CASTLE               ');
    console.log('\n            \x1b[1;37;41mEnter z to start a New Game\x1b[0m');
    console.log('                  Type q to Quit\n');
    var go = readline.question('###################################################\n');
    return go;
}
function recursiveTryFloors(player) {
    console.log('How many floors are you willing to climb ?');
    var floorsNumber = readline.question('\x1b[34m10, 20, 50\x1b[0m or\x1b[0m \x1b[34m100\x1b[0m ?\n');
    if (floorsNumber === '10') {
        return (parseInt(floorsNumber));
    }
    else if (floorsNumber === '20') {
        return (parseInt(floorsNumber));
    }
    else if (floorsNumber === '50') {
        return (parseInt(floorsNumber));
    }
    else if (floorsNumber === '100') {
        return (parseInt(floorsNumber));
    }
    else {
        console.clear();
        console.log("You can't build your own castle \u001B[1;32m".concat(player.name, "\u001B[0m !\n"));
        (0, sleep_1["default"])(1000);
        recursiveTryFloors(player);
    }
}
function choicesScreen(go, player) {
    if (go === 'z') {
        console.clear();
        console.log("                    Welcome \u001B[1;32m".concat(player.name, "\u001B[0m !\n"));
        (0, sleep_1["default"])(2000);
        console.log('You are about to go on an adventure to free princess Zelda.');
        console.log("You will have to climb the castles' floors all the way up to the tower where \u001B[1;31ma boss\u001B[0m lies.");
        console.log('On each floor, you will find an enemy that you will have to defeat in order to move on to the next floor.\n');
        (0, sleep_1["default"])(4000);
        console.log("So \u001B[1;32m".concat(player.name, "\u001B[0m, here are your options:"));
        var difficultyLevels = ['1- Normal', '2- Difficult', '3- Insane'];
        console.log("\n\u001B[34m".concat(difficultyLevels.join(' '), "\u001B[0m\n"));
        var difficultyAnswer = readline.question('How brave are you feeling today ?..\n').toLowerCase();
        var level = 0;
        if (difficultyAnswer === 'normal' || '1') {
            level = 1;
        }
        else if (difficultyAnswer === 'difficult' || '2') {
            level = 2;
        }
        else if (difficultyAnswer === 'insane' || '3') {
            level = 3;
        }
        else {
            functions_1["default"].endAdventure();
        }
        console.clear();
        console.log('Good !\n');
        var floorsNumber = recursiveTryFloors(player);
        console.log('\nExcellent !');
        var areYouReady = readline.keyInYN('Are you ready ?\n');
        if (areYouReady === true) {
            console.clear();
            console.log('Oh !');
            console.log('I forgot...');
            (0, sleep_1["default"])(2000);
            console.log("\nHere are \u001B[1;33m".concat(player.coins, "\u001B[0m coins for you. You will earn one more for each victory."));
            (0, sleep_1["default"])(3000);
            console.clear();
            console.log('Good luck !');
            (0, sleep_1["default"])(2000);
            var areYouReadyNum = areYouReady ? 1 : 0;
            var readyAndLevelAndFloors = [areYouReadyNum, level, floorsNumber];
            return readyAndLevelAndFloors;
        }
        else {
            functions_1["default"].endAdventure();
        }
    }
    else if (go === 'q') {
        process.exit();
    }
    else {
        functions_1["default"].endAdventure();
    }
}
exports["default"] = { startScreen: startScreen, choicesScreen: choicesScreen, recursiveTryFloors: recursiveTryFloors };
