"use strict";
exports.__esModule = true;
var sleep_1 = require("./sleep");
var readline = require('readline-sync');
function level(player) {
    var minXP = 15;
    var maxXP = 50;
    var randomXP = Math.floor(Math.random() * (maxXP - minXP)) + minXP;
    console.log("\nYou just won ".concat(randomXP, "XP"));
    player.levelup += randomXP;
    if (player.levelup >= 50) {
        (0, sleep_1["default"])(1000);
        console.log("You just passed a level!");
        console.log("What do you want to increase?\n");
        var levelUpActions = ['1- Attack🗡', '2- Heal💚'];
        for (var _i = 0, levelUpActions_1 = levelUpActions; _i < levelUpActions_1.length; _i++) {
            var element = levelUpActions_1[_i];
            console.log(element);
        }
        var levelUpAnswer = readline.question('');
        if (levelUpAnswer === "1" || levelUpAnswer.toLowerCase() === 'attack') {
            player.str += 5;
            console.clear();
            console.log("\nF\u00E9licitations\uD83E\uDD56! You now have ".concat((player.str), "\uD83D\uDDE1"));
        }
        else if (levelUpAnswer === "2" || levelUpAnswer.toLowerCase() === 'heal') {
            player.maxHp += 10;
            console.clear();
            console.log("\nCongratulations! Your HP max got upped by 10HP.");
        }
        player.levelup = 0;
        return;
    }
}
exports["default"] = level;
