"use strict";
exports.__esModule = true;
var sleep_1 = require("./sleep");
var functions_1 = require("./functions");
var level_and_experience_1 = require("./level_and_experience");
var readline = require('readline-sync');
function combats(floor, maxFloors, player, enemy, actions, escape) {
    var protectstr = Math.round(enemy.str / 2);
    var protectOnOff = 0;
    escape = 0;
    if (floor === maxFloors) {
        console.clear();
        console.log("\u001B[1;30;47m############### FINAL BOSS ###############\u001B[0m\n");
    }
    else {
        console.clear();
        console.log("\u001B[1;30;47m############### FLOOR ".concat(floor, " ###############\u001B[0m\n"));
    }
    console.log("You meet \u001B[31m".concat(enemy.name, "\u001B[0m."));
    (0, sleep_1["default"])(2000);
    while (enemy.hp > 0) {
        console.log("\n\u001B[31m".concat(enemy.name, "\u001B[0m:"));
        console.log("HP", functions_1["default"].lifeBarEnemy(enemy.hp, enemy.maxHp));
        console.log("\n\u001B[32m".concat(player.name, "\u001B[0m:"));
        console.log("HP", functions_1["default"].lifeBar(player.hp, player.maxHp));
        console.log("\nYour turn:\n");
        for (var _i = 0, actions_1 = actions; _i < actions_1.length; _i++) {
            var element = actions_1[_i];
            console.log(element);
        }
        var action = readline.question('What do you do ?\n');
        if (action === "1" || action.toLowerCase() === 'attack') {
            console.clear();
            enemy.hp = Math.max(0, enemy.hp - player.str);
            console.log("Well done ! Your enemy loses ".concat(player.str, " HP."));
        }
        else if (action === "2" || action.toLowerCase() === 'heal') {
            player.hp += (player.maxHp / 2);
            if (player.hp > player.maxHp) {
                player.hp = player.maxHp;
            }
            else {
                player.hp = player.hp;
            }
            console.clear();
            console.log("Ahhhh ! A good potion warms your heart. You gain ".concat(player.maxHp / 2, " HP."));
        }
        else if (action === "3" || action.toLowerCase() === "escape") {
            console.clear();
            console.log("\nYou have decided to escape...");
            console.log("You look for another room.");
            (0, sleep_1["default"])(2000);
            escape = 1;
            return (escape);
        }
        else if (action === "4" || action.toLowerCase() === "protect") {
            protectOnOff = 1;
            console.clear();
            console.log('You protect yourself from the next attack : the damage will be reduced by 50%!');
        }
        else {
            console.clear();
            console.log("Focus \u001B[32m".concat(player.name, "\u001B[0m. Do something!"));
        }
        if (enemy.hp > 0) {
            console.log("\u001B[31m".concat(enemy.name, "\u001B[0m attacks...\n"));
            (0, sleep_1["default"])(2000);
            if (protectOnOff === 1) {
                player.hp -= protectstr;
            }
            else {
                player.hp -= enemy.str;
            }
            if (player.hp > 0) {
                protectOnOff === 1 ? console.log("Ouch ! You lose ".concat(protectstr, " HP.")) : console.log("Ouch ! You lose ".concat(enemy.str, " HP."));
                protectOnOff = 0;
            }
            else {
                (0, sleep_1["default"])(2000);
                console.log("\u001B[31m".concat(enemy.name, "\u001B[0m:"));
                console.log("HP", functions_1["default"].lifeBarEnemy(enemy.hp, enemy.maxHp));
                console.log("\n\u001B[32m".concat(player.name, "\u001B[0m:"));
                console.log("HP", functions_1["default"].lifeBar(player.hp, player.maxHp));
                (0, sleep_1["default"])(2000);
                console.log("\nYou are DEAD :,(");
                console.log('\n############### GAMEOVER ###############\n');
                process.exit();
            }
        }
        else {
            if (floor !== maxFloors) {
                (0, sleep_1["default"])(2000);
                console.log("\n\u001B[31m".concat(enemy.name, "\u001B[0m:"));
                console.log("HP", functions_1["default"].lifeBarEnemy(enemy.hp, enemy.maxHp));
                console.log("\n\u001B[32m".concat(player.name, "\u001B[0m:"));
                console.log("HP", functions_1["default"].lifeBar(player.hp, player.maxHp));
                (0, sleep_1["default"])(1000);
                console.log("\nYou defeated \u001B[31m".concat(enemy.name, "\u001B[0m, yay !"));
                player.coins += 1;
                (0, sleep_1["default"])(1000);
                console.log("\nYou now possess \u001B[1;33m".concat(player.coins, "\u001B[0m coins"));
                (0, level_and_experience_1["default"])(player);
                if (floor + 1 !== maxFloors) {
                    (0, sleep_1["default"])(2000);
                    console.log("\n>>> You move on to floor ".concat((floor + 1), "."));
                    (0, sleep_1["default"])(2000);
                    return (0);
                }
                else {
                    (0, sleep_1["default"])(2000);
                    console.log('\n>>> OMG! You move on to the FINAL BOSS.');
                    (0, sleep_1["default"])(2000);
                    return (0);
                }
            }
            else {
                console.clear();
                console.log('###################################################################');
                console.log("\u001B[37;131m################## You defeated \u001B[31m".concat(enemy.name, "\u001B[0m ! Princess Zelda is free ! ##################"));
                console.log("(##You also leave with \u001B[1;33m".concat(player.coins, "\u001B[0m coins, you can buy her a chicken or something).##"));
                console.log('####################################################################################');
                process.exit();
            }
        }
    }
}
exports["default"] = combats;
