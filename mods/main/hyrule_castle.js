"use strict";
exports.__esModule = true;
var better_combat_options_1 = require("../functions/better_combat_options");
var basic_customization_1 = require("../functions/basic_customization");
var functions_1 = require("../functions/functions");
var fs = require('fs');
var allPlayersString = fs.readFileSync('../json/players.json', 'utf-8');
var allBossesString = fs.readFileSync('../json/bosses.json', 'utf-8');
var allEnemiesString = fs.readFileSync('../json/enemies.json', 'utf-8');
var allPlayers = JSON.parse(allPlayersString);
var allBosses = JSON.parse(allBossesString);
var allEnemies = JSON.parse(allEnemiesString);
var rarityPercentages = [50, 30, 15, 4, 1];
var rarityIndexPlayers = functions_1["default"].rarityIndex(rarityPercentages);
var rarityIndexBosses = functions_1["default"].rarityIndex(rarityPercentages);
var rarityIndexEnemies = functions_1["default"].rarityIndex(rarityPercentages);
var rarityPlayers = functions_1["default"].rarityCharacters(allPlayers, rarityIndexPlayers);
var player = functions_1["default"].rarityPick(rarityPlayers);
var go = basic_customization_1["default"].startScreen();
player.coins = 12;
var readyAndLevelAndFloors = basic_customization_1["default"].choicesScreen(go, player);
var ready = readyAndLevelAndFloors[0];
var level = readyAndLevelAndFloors[1];
var maxFloors = readyAndLevelAndFloors[2];
functions_1["default"].difficultyStats(level, allEnemies, allBosses);
var rarityBosses = functions_1["default"].rarityCharacters(allBosses, rarityIndexBosses);
var boss = functions_1["default"].rarityPick(rarityBosses);
var rarityEnemies = functions_1["default"].rarityCharacters(allEnemies, rarityIndexEnemies);
var enemy = functions_1["default"].rarityPick(rarityEnemies);
player.maxHp = player.hp;
player.levelup = 0;
boss.maxHp = boss.hp;
var actions = ['1- Attack🗡', '2- Heal💚', '3- Escape🏃', '4- Protect🛡'];
function main(go, enemy) {
    var floor = 1;
    var progression = 1;
    var escape = 0;
    if (go === 1) {
        while (floor !== maxFloors) {
            if (progression === 10) {
                progression = 0;
                enemy = functions_1["default"].rarityPick(functions_1["default"].rarityCharacters(JSON.parse(allBossesString), functions_1["default"].rarityIndex(rarityPercentages)));
            }
            else {
                enemy.maxHp = enemy.hp;
                escape = (0, better_combat_options_1["default"])(floor, maxFloors, player, enemy, actions, escape);
                if (escape === 0) {
                    floor += 1;
                    progression += 1;
                }
                else {
                    escape = 0;
                }
                enemy.hp = enemy.maxHp;
                enemy = functions_1["default"].rarityPick(functions_1["default"].rarityCharacters(JSON.parse(allEnemiesString), functions_1["default"].rarityIndex(rarityPercentages)));
            }
        }
    }
    else {
        functions_1["default"].endAdventure();
    }
    (0, better_combat_options_1["default"])(floor, maxFloors, player, boss, actions, escape);
    process.exit();
}
main(ready, enemy);
