CharactersInterface from './interface';
import combat from './combat';
import start from './start';

const readline = require('readline-sync');
const fs = require('fs');

const allPlayersString: string = fs.readFileSync('./players.json', 'utf-8');
const allEnemiesString: string = fs.readFileSync('./enemies.json', 'utf-8');
const allBossesString: string = fs.readFileSync('./bosses.json', 'utf-8');

const allPlayers: CharactersInterface[] = JSON.parse(allPlayersString);
const allEnemies: CharactersInterface[] = JSON.parse(allEnemiesString);
const allBosses: CharactersInterface[] = JSON.parse(allBossesString);

let numberOfFloors: number = 10;
const actions: string[] = [ 'Attack', 'Heal'];

const go = start.startScreen();

const player: CharactersInterface = allPlayers[0];
const enemy: CharactersInterface = allEnemies[11];
const boss: CharactersInterface = allBosses[0];

const playerMaxHp = player.hp;
const enemyMaxHp = enemy.hp;
const bossMaxHp = boss.hp;

const ready = start.characterScreen(go, player);

function Floors(ready, numberOfFloors) {
  if (ready === true) {
    let whichFloor = 1;
    while (whichFloor !== numberOfFloors && player.hp !== 0) {
      combat.regCombat(whichFloor, player, enemy, playerMaxHp, enemyMaxHp, actions);
      whichFloor += 1;
    }
  } else {
    console.log('\nGo train and come back when you are more prepared...\n');
    return;
  }
  if (player.hp !== 0) {
    combat.bossCombat(player, boss, playerMaxHp, bossMaxHp, actions);
    return;
  }
}

Floors(ready, numberOfFloors);



