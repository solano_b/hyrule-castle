"use strict";
exports.__esModule = true;
var combat_1 = require("./combat");
var start_1 = require("./start");
var readline = require('readline-sync');
var fs = require('fs');
var allPlayersString = fs.readFileSync('./players.json', 'utf-8');
var allEnemiesString = fs.readFileSync('./enemies.json', 'utf-8');
var allBossesString = fs.readFileSync('./bosses.json', 'utf-8');
var allPlayers = JSON.parse(allPlayersString);
var allEnemies = JSON.parse(allEnemiesString);
var allBosses = JSON.parse(allBossesString);
var numberOfFloors = 10;
var actions = ['Attack', 'Heal'];
var go = start_1["default"].startScreen();
var player = allPlayers[0];
var enemy = allEnemies[11];
var boss = allBosses[0];
var playerMaxHp = player.hp;
var enemyMaxHp = enemy.hp;
var bossMaxHp = boss.hp;
var ready = start_1["default"].characterScreen(go, player);
function Floors(ready, numberOfFloors) {
    if (ready === true) {
        var whichFloor = 1;
        while (whichFloor !== numberOfFloors && player.hp !== 0) {
            combat_1["default"].regCombat(whichFloor, player, enemy, playerMaxHp, enemyMaxHp, actions);
            whichFloor += 1;
        }
    }
    else {
        console.log('\nGo train and come back when you are more prepared...\n');
        return;
    }
    if (player.hp !== 0) {
        combat_1["default"].bossCombat(player, boss, playerMaxHp, bossMaxHp, actions);
        return;
    }
}
Floors(ready, numberOfFloors);
