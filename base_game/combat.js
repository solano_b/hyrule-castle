"use strict";
exports.__esModule = true;
var readline = require('readline-sync');
function regCombat(floor, player, enemy, maxHpPlayer, maxHpEnemy, actions) {
    console.log("\n############### FLOOR ".concat(floor, " ###############\n"));
    console.log("You meet ".concat(enemy.name, "."));
    enemy.hp = maxHpEnemy;
    while (enemy.hp > 0) {
        var action = readline.keyInSelect(actions, '\nWhat do you do ?');
        if (action === 0) {
            enemy.hp = Math.max(0, enemy.hp - player.str);
            console.log("\nWell done ! Your enemy now has ".concat(enemy.hp, " HP."));
        }
        else if (action === 1) {
            player.hp += (maxHpPlayer / 2);
            if (player.hp > maxHpPlayer) {
                player.hp = maxHpPlayer;
            }
            else {
                player.hp = player.hp;
            }
            console.log("\nAhhhh ! Feels nice, doesn't it ? You now have ".concat(player.hp, " HP."));
        }
        else {
            console.log("Focus ".concat(player.name, ". This is not an action."));
        }
        if (enemy.hp > 0) {
            console.log("\n".concat(enemy.name, " attacks !\n"));
            player.hp -= enemy.str;
            if (player.hp <= 0) {
                console.log("You now have 0 HP : you are DEAD :,(");
                console.log('############### GAMEOVER ###############\n');
                return;
            }
            else {
                console.log("You now have ".concat(player.hp, " HP. Your turn."));
            }
        }
        else {
            console.log("\nYou defeated ".concat(enemy.name, ", yay !"));
            console.log("You move on to floor ".concat((floor + 1), "."));
            return;
        }
    }
}
function bossCombat(player, boss, maxHpPlayer, maxHpBoss, actions) {
    console.log("\n############### FINAL BOSS ###############\n");
    console.log("You meet ".concat(boss.name, "."));
    boss.hp = maxHpBoss;
    while (boss.hp > 0) {
        var action = readline.keyInSelect(actions, '\nWhat do you do ?');
        if (action === 0) {
            boss.hp = Math.max(0, boss.hp - player.str);
            console.log("\nWell done ! ".concat(boss.name, " now has ").concat(boss.hp, " HP."));
        }
        else if (action === 1) {
            player.hp += (maxHpPlayer / 2);
            if (player.hp > maxHpPlayer) {
                player.hp = maxHpPlayer;
            }
            else {
                player.hp = player.hp;
            }
            console.log("Ahhhh ! Feels nice, doesn't it ? You now have ".concat(player.hp, " HP."));
        }
        else {
            console.log("Focus ".concat(player.name, ". This is not an action."));
        }
        if (boss.hp > 0) {
            console.log("\n".concat(boss.name, " attacks !\n"));
            player.hp -= boss.str;
            if (player.hp <= 0) {
                console.log("You now have 0 HP : you are DEAD :,(");
                console.log('############### GAMEOVER ###############\n');
                return;
            }
            else {
                console.log("You now have ".concat(player.hp, " HP. Your turn."));
            }
        }
        else {
            console.log("\n\u001B[37;131m########## You defeated ".concat(boss.name, " ! Princess Zelda is free ! ##########\n"));
            return;
        }
    }
}
exports["default"] = { regCombat: regCombat, bossCombat: bossCombat };
