const readline = require('readline-sync');

function regCombat(floor: number, player: any, enemy: any, maxHpPlayer: number, maxHpEnemy: number, actions: string[]) {
  console.log(`\n############### FLOOR ${floor} ###############\n`);
  console.log(`You meet ${enemy.name}.`);
  enemy.hp = maxHpEnemy;
  while (enemy.hp > 0) {
    let action: number = readline.keyInSelect(actions, '\nWhat do you do ?');
    if (action === 0) {
      enemy.hp = Math.max(0, enemy.hp - player.str);
      console.log(`\nWell done ! Your enemy now has ${enemy.hp} HP.`);
    } else if (action === 1) {
      player.hp += (maxHpPlayer / 2);
      if (player.hp > maxHpPlayer) {
        player.hp = maxHpPlayer;
      } else {
        player.hp = player.hp;
      }
      console.log(`\nAhhhh ! Feels nice, doesn't it ? You now have ${player.hp} HP.`);
    } else {
      console.log(`\Focus ${player.name}. This is not an action.`);
    }
    if (enemy.hp > 0) {
      console.log(`\n${enemy.name} attacks !\n`);
      player.hp -= enemy.str;
      if (player.hp <= 0) {
        console.log(`You now have 0 HP : you are DEAD :,(`);
        console.log('############### GAMEOVER ###############\n');
        return;
      } else {
        console.log(`You now have ${player.hp} HP. Your turn.`);
      }
    } else {
      console.log(`\nYou defeated ${enemy.name}, yay !`);
      console.log(`You move on to floor ${(floor + 1)}.`);
      return;
    }
  }
}

function bossCombat(player: any, boss: any, maxHpPlayer: number, maxHpBoss: number, actions: string[]) {
  console.log(`\n############### FINAL BOSS ###############\n`);
  console.log(`You meet ${boss.name}.`);
  boss.hp = maxHpBoss;
  while (boss.hp > 0) {
    let action: number = readline.keyInSelect(actions, '\nWhat do you do ?');
    if (action === 0) {
      boss.hp = Math.max(0, boss.hp - player.str);
      console.log(`\nWell done ! ${boss.name} now has ${boss.hp} HP.`);
    } else if (action === 1) {
      player.hp += (maxHpPlayer / 2);
      if (player.hp > maxHpPlayer) {
        player.hp = maxHpPlayer;
      } else {
        player.hp = player.hp;
      }
      console.log(`Ahhhh ! Feels nice, doesn't it ? You now have ${player.hp} HP.`);
    } else {
      console.log(`Focus ${player.name}. This is not an action.`);
    }
    if (boss.hp > 0) {
      console.log(`\n${boss.name} attacks !\n`);
      player.hp -= boss.str;
      if (player.hp <= 0) {
        console.log(`You now have 0 HP : you are DEAD :,(`);
        console.log('############### GAMEOVER ###############\n');
        return;
      } else {
        console.log(`You now have ${player.hp} HP. Your turn.`);
      }
    } else {
      console.log(`\n\x1b[37;131m########## You defeated ${boss.name} ! Princess Zelda is free ! ##########\n`);
      return;
    }
  }
}

export default { regCombat, bossCombat };