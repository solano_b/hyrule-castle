"use strict";
exports.__esModule = true;
var readline = require('readline-sync');
function startScreen() {
    console.log('############### \x1b[31mTHE LEGEND OF ZELDA\x1b[0m ###############');
    console.log('                   \x1b[31mHYRULE CASTLE\x1b[0m               ');
    console.log('\n                 \x1b[1;39;47mEnter \x1b[0m\x1b[1;31;47mGo!\x1b[0m\x1b[1;39;47m to start\x1b[0m\x1b[0m\n');
    var go = readline.question('###################################################\\n');
    return go;
}
function characterScreen(go, player) {
    if (go === 'Go!') {
        console.log("\n                    Welcome ".concat(player.name, " !\n"));
        console.log('You are about to go on an adventure to free princess Zelda.');
        console.log('You will have to climb ten floors to reach the tower where Ganondorf lies.');
        console.log('On each floor, you will find an enemy that you will need to defeat in order to move on to the next floor.\n');
        var ready = readline.keyInYN('Are you ready ?');
        return ready;
    }
    else {
        console.error('\nWhy are you hesitant?');
        return;
    }
}
exports["default"] = { startScreen: startScreen, characterScreen: characterScreen };
